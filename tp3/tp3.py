#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

@author: cverdier
"""

import autograd as ag
import autograd.numpy as np
from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt

## Question 1

print("Question 1\n")
def f (a,b):
    return a**3 + 2*a**2 - 2*a*b + b**2 + a*b**3 - 2*b + 5

fig = plt.figure(figsize = (20,20))
ax = plt.axes(projection='3d')

ax.set_xlabel('$a$', labelpad=20)
ax.set_ylabel('$b$', labelpad=20)
ax.set_zlabel('$f(a,b)$', labelpad=20)

aplot = np.arange (-2.5, 2, 0.05)
bplot = np.arange (-2.5, 3, 0.05)

A, B = np.meshgrid (aplot, bplot)
Z = f(A,B)
Z = np.clip (Z, 3, 13)

ax.plot_surface(A, B, Z, cmap="spring_r", lw=0.5, rstride=1, cstride=1, alpha=0.5)
ax.contour(A, B, Z, 30, colors="k", linestyles="dotted")

plt.show ()

print("On remarque que les points stationnaires sont : \n(-1.5, 0.5) \n(0.2, 1) \n(0.7, -2) \n(-1.25, -0.5) \n(-0.2, 1.5)\n")

## Question 2

def nabla_f(a,b):
    return np.array([3*a**2 + 4*a -2*b + b**3 , -2*a + 2*b + 3*a*b**2 - 2])

def H_f(a, b):
    return np.array([[6*a + 4, -2 + 3*b**2], [-2 + 3*b**2, 2 + 6*a*b]])

## Question 3

print("\nQuestion 3\n")

print ("\ncalcule des coordonées précises des points stationnaires.\n")

print("\npoint(-1.5, 0.5):\n")
a = -1.5
b = 0.5

u = np.array ([a,b], dtype=np.float64)

for n in range (5) :
    a = u[0]
    b = u[1]
    print ('\nu[%d] =' % n, u)
    h = np.linalg.solve (- H_f (a,b), nabla_f (a,b))
    u = u + h
    
a = u[0]
b = u[1]
print (" (%lf, %lf)\n" % (a,b))


print("\npoint (0.2, 1):\n")
a = 0.2
b = 1

u = np.array ([a,b], dtype=np.float64)

for n in range (5) :
    a = u[0]
    b = u[1]
    print ('\nu[%d] =' % n, u)
    h = np.linalg.solve (- H_f (a,b), nabla_f (a,b))
    u = u + h
    
a = u[0]
b = u[1]
print ("\npoint (%lf, %lf)\n" % (a,b))


print("\npoint (0.7, -2):")
a = 0.7
b = -2

u = np.array ([a,b], dtype=np.float64)

for n in range (5) :
    a = u[0]
    b = u[1]
    print ('\nu[%d] =' % n, u)
    h = np.linalg.solve (- H_f (a,b), nabla_f (a,b))
    u = u + h
    
a = u[0]
b = u[1]
print ("\n point (%lf, %lf)\n" % (a,b))


print("\npoint (-1.25, -0.5):")
a = -1.25
b = -0.5

u = np.array ([a,b], dtype=np.float64)

for n in range (5) :
    a = u[0]
    b = u[1]
    print ('\nu[%d] =' % n, u)
    h = np.linalg.solve (- H_f (a,b), nabla_f (a,b))
    u = u + h
    
a = u[0]
b = u[1]
print ("\npoint (%lf, %lf)\n" % (a,b))


print("point (-0.2, 1.5):")
a = -0.2
b = 1.5

u = np.array ([a,b], dtype=np.float64)

for n in range (5) :
    a = u[0]
    b = u[1]
    print ('\nu[%d] =' % n, u)
    h = np.linalg.solve (- H_f (a,b), nabla_f (a,b))
    u = u + h
    
a = u[0]
b = u[1]
print ("\npoint (%lf, %lf)\n" % (a,b))

## Question 4

print("\nQuestion 4\n")
print("Nous allons verifier que pour être minimum local, un point stationnaire doit avoir une matrice hessienne dont les valeurs propres sont positives:")

print ("\n point (-1.563091, 0.747704):")
values, vectors = np.linalg.eigh(H_f(-1.56309084,  0.74770381))
print("VP :", values)

print ("\n point (0.225501, 0.931808):")
values, vectors = np.linalg.eigh(H_f(0.225501, 0.931808))
print("VP :", values)

print ("\npoint (0.620107, -1.962567):")
values, vectors = np.linalg.eigh(H_f(0.620107, -1.962567))
print("VP :", values)

print ("\npoint (-1.238526, -0.179000):")
values, vectors = np.linalg.eigh(H_f(-1.238526, -0.179000))
print("VP :", values)

print ("\npoint (-0.211311, 1.566541:")
values, vectors = np.linalg.eigh(H_f(-0.211311, 1.566541))
print("VP :", values)

print ("\nLe point (0.225501, 0.931808) est donc minimum local.")


## Question 5

print ("\nQuestion 5\n")

def fbis (u):
    a=u[0]
    b=u[1]
    return a**3 + 2*a**2 - 2*a*b + b**2 + a*b**3 - 2*b + 5


print ("calcule des coordonnées précises des points stationnaires en utilisant grad et hessian.\n")

print(" point (-1.5, 0.5):")
a = -1.5
b = 0.5

u = np.array ([a,b], dtype=np.float64)

for n in range (5) :
    print ('\nu[%d] =' % n, u)
    h = np.linalg.solve (- ag.hessian(fbis)(u), ag.grad(fbis)(u))
    u = u + h
    
a = u[0]
b = u[1]
print ("\npoint (%lf, %lf)\n" % (a,b))


print("\npoint (0.2, 1):")
a = 0.2
b = 1

u = np.array ([a,b], dtype=np.float64)

for n in range (5) :
    print ('\nu[%d] =' % n, u)
    h = np.linalg.solve (- ag.hessian(fbis)(u), ag.grad(fbis)(u))
    u = u + h
    
a = u[0]
b = u[1]
print ("\n le point (%lf, %lf)\n" % (a,b))


print("\npoint (0.7, -2):")
a = 0.7
b = -2

u = np.array ([a,b], dtype=np.float64)

for n in range (5) :
    print ('\nu[%d] =' % n, u)
    h = np.linalg.solve (- ag.hessian(fbis)(u), ag.grad(fbis)(u))
    u = u + h
    
a = u[0]
b = u[1]
print ("\n le point (%lf, %lf)\n" % (a,b))


print("\n point (-1.25, -0.5):")
a = -1.25
b = -0.5

u = np.array ([a,b], dtype=np.float64)

for n in range (5) :
    print ('\nu[%d] =' % n, u)
    h = np.linalg.solve (- ag.hessian(fbis)(u), ag.grad(fbis)(u))
    u = u + h
    
a = u[0]
b = u[1]
print ("\n point (%lf, %lf)\n" % (a,b))


print("\n point (-0.2, 1.5):")
a = -0.2
b = 1.5

u = np.array ([a,b], dtype=np.float64)

for n in range (5) :
    print ('\nu[%d] =' % n, u)
    h = np.linalg.solve (- ag.hessian(fbis)(u), ag.grad(fbis)(u))
    u = u + h
    
a = u[0]
b = u[1]
print ("\n le point (%lf, %lf)\n" % (a,b))

print ("\nOn re trouve bien les mêmes résultats que précédemment.\n")

##Question 6

print ("\nQuestion 6\n")

def nabla_fter(u) :
    a = u[0]
    b = u[1]
    t1 = b ** 2
    t2 = a ** 2
    t3 = a * t1 * b - 2 * a * b + t2 * a - 2 * b + t1 + 2 * t2 + 5

    df_dt3 = 1
    df_dt2 = df_dt3 * (a + 2)
    df_dt1 = df_dt3 * (a * b + 1)
    df_da = df_dt2 * (2*a) + df_dt3 * (t1*b - 2*b + t2 )
    df_db = df_dt1 * (2*b) + df_dt3 * (a*t1 - 2*a - 2)

    return np.array([df_da, df_db], dtype = np.float64)


print ("On calcule les coordonnées précises des points stationnaires en utilisant nabla_fter et hessian.\n")


print(" point (-1.5, 0.5):")
a = -1.5
b = 0.5

u = np.array ([a,b], dtype=np.float64)

for n in range (5) :
    print ('\nu[%d] =' % n, u)
    h = np.linalg.solve (- ag.hessian(fbis)(u), nabla_fter(u))
    u = u + h
    
a = u[0]
b = u[1]
print ("\npoint (%lf, %lf)\n" % (a,b))


print("\n point (0.2, 1):")
a = 0.2
b = 1

u = np.array ([a,b], dtype=np.float64)

for n in range (5) :
    print ('\nu[%d] =' % n, u)
    h = np.linalg.solve (- ag.hessian(fbis)(u), nabla_fter(u))
    u = u + h
    
a = u[0]
b = u[1]
print ("\n  point (%lf, %lf)\n" % (a,b))


print("\n point (0.7, -2):")
a = 0.7
b = -2

u = np.array ([a,b], dtype=np.float64)

for n in range (5) :
    print ('\nu[%d] =' % n, u)
    h = np.linalg.solve (- ag.hessian(fbis)(u), nabla_fter(u))
    u = u + h
    
a = u[0]
b = u[1]
print ("\npoint (%lf, %lf)\n" % (a,b))


print("\n point (-1.25, -0.5):")
a = -1.25
b = -0.5

u = np.array ([a,b], dtype=np.float64)

for n in range (5) :
    print ('\nu[%d] =' % n, u)
    h = np.linalg.solve (- ag.hessian(fbis)(u), nabla_fter(u))
    u = u + h
    
a = u[0]
b = u[1]
print ("\n point (%lf, %lf)\n" % (a,b))


print("\n point (-0.2, 1.5):")
a = -0.2
b = 1.5

u = np.array ([a,b], dtype=np.float64)

for n in range (5) :
    print ('\nu[%d] =' % n, u)
    h = np.linalg.solve (- ag.hessian(fbis)(u), nabla_fter(u))
    u = u + h
    
a = u[0]
b = u[1]
print ("\n point (%lf, %lf)\n" % (a,b))


print ("On trouve bien les mêmes résultats que précédemment.")







