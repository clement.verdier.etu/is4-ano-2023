#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 22 10:22:47 2023

@author: cverdier
"""
import autograd as ag
import autograd.numpy as np


from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt


# Question 1
print('\n''question 1''\n')

def f(a,b):
    return a**3 * b - 3 * a**2 * (b-1) + b**2 - 1

def g(a,b):
    return a**2 * b**2 - 2


# Calculés avec Maple
abscisses = [-1.896238074, -0.4638513741, 1.568995999]
ordonnees = [0.6844390650, -0.8460057970, -0.4658228710]

a_min, a_max = -3.05,3.5 
b_min, b_max = -3.5, 2.5

fig = plt.figure(figsize = (20,10))
ax = fig.add_subplot(1, 2, 1, projection='3d')

ax.set_xlabel('$a$', labelpad=20)
ax.set_ylabel('$b$', labelpad=20)
ax.set_zlabel('$f(a,b)$', labelpad=20)

aplot = np.arange (a_min, a_max, 0.1)
bplot = np.arange (b_min, b_max, 0.1)

A, B = np.meshgrid (aplot, bplot)
Z = f(A,B)

ax.plot_surface(A, B, Z, cmap="spring_r", lw=0.5, rstride=1, cstride=1, alpha=0.5)
ax.contour(A, B, Z, 10, colors="k", linestyles="dashed")
ax.contour(A, B, Z, 0, colors="blue",  levels=np.array([0], dtype=np.float64), linestyles="solid")

for i in range (len(abscisses)) :
    ax.scatter (abscisses[i], ordonnees[i], f(abscisses[i], ordonnees[i]), color='black') # 
    
#########################################################
# 2ème sous-graphe


ax.set_xlabel('$a$', labelpad=20)
ax.set_ylabel('$b$', labelpad=20)
ax.set_zlabel('$g(a,b)$', labelpad=20)

aplot = np.arange (a_min, a_max, 0.1)
bplot = np.arange (b_min, b_max, 0.1)

A, B = np.meshgrid (aplot, bplot)
Z = g(A,B)

ax.plot_surface(A, B, Z, cmap="autumn_r", lw=0.5, rstride=1, cstride=1, alpha=0.5)
ax.contour(A, B, Z, 10, colors="k", linestyles="dashed")
ax.contour(A, B, Z, 0, colors="green",  levels=np.array([0], dtype=np.float64), linestyles="solid")

for i in range (len(abscisses)) :
    ax.scatter (abscisses[i], ordonnees[i], g(abscisses[i], ordonnees[i]), color='black')

# plt.savefig ("../deux-variables-graphe.png")

plt.show()

print('Ainsi graphiquement on observe que ce sont pour les valeurs suivantes que les deux fonction s annulent :\n - a = -0.8 et b = 1.92 \n - a = -2.9 et b = -0.5') 
    
# Question 2 : matrice Jacobienne

def Jac (a,b) :
    return np.array ([[3 * a**2 * b - 6 * a * (b - 1), a**3 - 3 * a**2 + 2 * b], [2 * a * b**2, 2 * a**2 * b]], dtype=np.float64)

# Question 3 : 
print('\n''question 3''\n')
    
def F(u) :
    a, b = u[0], u[1]
    return np.array ([a**3 * b - 3 * a**2 * (b - 1) + b**2 - 1, a**2 * b**2 - 2])

def J_F(u) :
    a, b = u[0], u[1]
    return np.array ([[3 * a**2 * b - 6 * a * (b - 1), a**3 - 3 * a**2 + 2 * b], [2 * a * b**2, 2 * a**2 * b]], dtype=np.float64)
print('\nvoici ce que l on trouve en calculant précisément les coordonnées des deux zéros de F par une méthode de Newton.\n')

u = np.array([-0.8,1.92], dtype=np.float64)

for n in range (0,7) :
    h = np.linalg.solve (- J_F(u), F(u))
    u = u + h
print(u,'\n')

u = np.array([-2.9,0.5], dtype=np.float64)

for n in range (0,7) :
    h = np.linalg.solve (- J_F(u), F(u))
    u = u + h
print(u)


# Question 4

import autograd.numpy as np
from autograd import jacobian


print('\n''question 4''\n')

print('\nvoici ce que l on trouve en calculant précisément les coordonnées des deux zéros de F par une méthode de Newton avec autograd.\n')

def F(u):
    a, b = u[0], u[1]
    return np.array([a**3*b - 3*a**2*(b - 1) + b**2 - 1, a**2*b**2 - 2])

# Utilisation de la fonction autograd.jacobian pour le calcul de la jacobienne

J_F = jacobian(F)

u = np.array([-0.8, 1.9], dtype=np.float64)

for n in range(0, 7):
    h = np.linalg.solve(-J_F(u), F(u))
    u = u + h

print(u,'\n')

def F(u):
    a, b = u[0], u[1]
    return np.array([a**3*b - 3*a**2*(b - 1) + b**2 - 1, a**2*b**2 - 2])

# Utilisation de la fonction autograd.jacobian pour le calcul de la jacobienne
J_F = jacobian(F)

u = np.array([-2.9,0.5], dtype=np.float64)

for n in range(0, 7):
    h = np.linalg.solve(-J_F(u), F(u))
    u = u + h

print(u)

# Question 5

print('\n''question 5''\n')
def F(u) :
    a = u[0]
    b = u[1]
    # Pour f(a,b)
    t1 = a ** 2
    t2 = b ** 2
    t3 = (a*b - 3*b + 3)*t1 + t2 - 1
    

    
    
    # Pour g(a,b)
    t1 = a ** 2
    t2 = b ** 2
    t4 = t1 * t2 - 2
    # Résultat
    return np.array ([t3, t4])



def j_F(u):
    a = u[0]
    b = u[1]
    # Pour f(a,b)
    t1 = a ** 2
    t2 = b ** 2
    t3 = (a*b - 3*b + 3)*t1 + t2 - 1
    
    dt1_da = 2 * a
    dt1_db = 0
    dt2_da = 0
    dt2_db = 2 * b
    dt3_da = b * t1 + (a * b - 3 * b + 3) *  dt1_da + dt2_da
    dt3_db = (a-3)*t1 + (a*b-3*b+3)*dt1_db + dt2_db
    
    # Pour g(a,b)
    t1 = a**2
    t2 = b**2
    t4 = t1*t2-2
    
    dt1_da = 2*a
    dt1_db = 0
    dt2_da = 0
    dt2_db = 2*b
    dt4_da = dt1_da * t2+t1 * dt2_da
    dt4_db = dt1_db*t2+t1*dt2_db
    
    return np.array([[dt3_da, dt3_db],[dt4_da,dt4_db]], dtype=np.float64)
    
    
    # Résultat
print('\nvoici ce que l on trouve en calculant précisément les coordonnées des deux zéros de F par une méthode forward.\n')

u = np.array([-0.8, 1.9], dtype=np.float64)

for n in range(0, 7):
    h = np.linalg.solve(-J_F(u), F(u))
    u = u + h

print(u,'\n')

u = np.array([-2.9,0.5], dtype=np.float64)
for n in range(0, 7):
    h = np.linalg.solve(-J_F(u), F(u))
    u = u + h

print(u)















