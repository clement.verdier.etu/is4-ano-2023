#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 17 15:27:30 2023

@author: cverdier
"""
import autograd as ag
import autograd.numpy as np
import numpy as np
import scipy.linalg as nla
import matplotlib.pyplot as plt

#1 - Moindres carrés linéaires

# Question 1. Reprendre les méthodes vues en cours sur un exemple un peu différent. La démarche est la suivante. On part de l’équation d’une cubique
print('\n''Question 1''\n')

def f(x, alpha, beta, gamma, mu):
    return alpha*x**3 + beta*x**2 + gamma*x + mu

# Les Paramètres :
alpha, beta, gamma, mu = 0.5, -2, 1, 7

# Les points expérimentaux :
    
Tx = np.array([-1.1, 0.17, 1.22, -0.5, 2.02, 1.81])
p = Tx.shape[0]
Ty_sur_la_courbe = np.array([f(x, alpha, beta, gamma, mu) for x in Tx])
perturbations = 0.5 * np.array([-1.3, 2.7, -5, 0, 1.4, 6])
Ty_experimentaux = Ty_sur_la_courbe + perturbations

# On calcul l'erreur initiale
erreur_initiale = nla.norm(Ty_sur_la_courbe - Ty_experimentaux, 2)

# Courbe initiale et les points expérimentaux
xplot = np.linspace(-1.2, 2.1, 100) # 100 points entre -1.2 et 2.1
yplot_initiale = [f(x, alpha, beta, gamma, mu) for x in xplot] #utilisation de la fonction defi,it

#trace le plot
plt.scatter(Tx, Ty_experimentaux, color='red', label='Points expérimentaux')
plt.plot(xplot, yplot_initiale, color='blue', label='Courbe initiale')
plt.title('Courbe initiale / points expérimentaux')
plt.legend()
plt.show()

# Construction du système d'équations linéaires Ax = b
A = np.empty([p, 4], dtype=np.float64)
b = Ty_experimentaux

for i in range(p):
    A[i, 0] = Tx[i]**3
    A[i, 1] = Tx[i]**2
    A[i, 2] = Tx[i]
    A[i, 3] = 1

# Nous allons maintenant resoudre le système linéaire
ATA = np.dot(np.transpose(A), A)
ATb = np.dot(np.transpose(A), b)

# Solution optimale
x_optimal = nla.solve(ATA, ATb)

# Mise à jour des paramètres
alpha, beta, gamma, mu = x_optimal

# On calcul la nouvelle erreur minimale issus des nouveaux paramètres
Ty_optimal = np.array([f(x, alpha, beta, gamma, mu) for x in Tx])
erreur_minimale = nla.norm(Ty_experimentaux - Ty_optimal, 2)

# Vérification que l'erreur minimale soit inférieure à l'erreur initiale
if erreur_minimale < erreur_initiale:
    print("Vrais : erreur_minimale < erreur_initiale")

# Tracer la courbe optimale en orange
yplot_optimal = [f(x, alpha, beta, gamma, mu) for x in xplot]

plt.scatter(Tx, Ty_experimentaux, color='red', label='Points expérimentaux')
plt.plot(xplot, yplot_initiale, color='blue', label='Courbe initiale')
plt.plot(xplot, yplot_optimal, color='orange', label='Courbe optimale')
plt.title('Courbe optimale / points expérimentaux')
plt.legend()
plt.show()

#2 - Méthode de Newton en une variable

# Question 2. Calculer les premiers termes d’une suite (un) qui tende vers √3 2 obtenue à partir d’un polynôme à coefficients entiers.
print('\n''question 2''\n')


def f(x):
    return x**3 - 2

def fprime(x):
    return 3 * x**2

u=2
for n in range (0, 6):
    print ('u[%d] =' % n, u)
    u = u-f(u)/fprime(u)
    
print ('On remarque bien que les valeurs de u vont rapidement converger  vers la racine cubique de 2')
    
#Question 3. Calculer les premiers termes d’une suite qui calcule le minimum local de la cubique optimale
#obtenue dans la section précédente. Au cas où vous auriez un exemple un peu différent, les coefficients sont
#alpha, beta, gamma, mu = 1.5870106105525719, -3.1447447637103476, -0.37473299354311346, 7.484053576868045

print('\n''question 3''\n')

alpha, beta, gamma, mu = 1.5870106105525719, -3.1447447637103476, -0.37473299354311346, 7.484053576868045


def f(x):
    return alpha*x**3 + beta*x**2 + gamma*x + mu

def fprime(x) :
    return 3*alpha*x**2 + 2*beta*x + gamma

def fseconde (x) :
    return 6*alpha*x + 2*beta
u = 0.8
for n in range (10) :
    print ('u[%d] ='% n, u)
    u = u - fprime(u)/fseconde(u)
    
print ('LOn remarque bien que u convergent vers le minimum local de la cubique ')

# Question 4. Même question, en utilisant les fonctions grad et éventuellement hessian du paquetage
# autograd.


print('\n''question 4''\n')

import autograd.numpy as np
from autograd import grad, hessian


def f(x):
    return alpha*x**3 + beta*x**2 + gamma*x + mu

# Calcul de la dérivée première et seconde
f_prime = grad(f)
f_second = hessian(f)

k = 0.8
for n in range(10):
    print('k[%d] =' % n, k)
    k = k - f_prime(k) / f_second(k)

k_rounded = round(k, 6)
u_rounded = round(u, 6)

if k_rounded == u_rounded:
    print('Les dernières valeurs de k et u arrondies au millième sont égales :', k_rounded ,' ainsi nous avons bien les memes résultats')

